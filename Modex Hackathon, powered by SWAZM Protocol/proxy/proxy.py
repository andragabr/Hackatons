import requests

import sys

if sys.version_info[0] == 3:
    from urllib.request import urlopen
else:
    # Not Python 3 - today, it is most likely to be Python 2
    # But note that this might need an update when Python 4
    # might be around one day
    from urllib2 import urlopen


from urllib2 import urlopen

from requests import get

proxy = { 
            "http" :  "http://194.182.64.67:3128",
            "https" :  "http://194.182.64.67:3128",
        }


ip = get('https://api.ipify.org').text
print('My public IP address is: {}'.format(ip))


ip = get('https://api.ipify.org', proxies=proxy).text
print('My public IP address is: {}'.format(ip))



print(urlopen("http://httpbin.org/ip").read())

r = requests.get('http://purepython.eaudeweb.ro/', proxies=proxy)
#p = requests.get('http://httpbin.org/ip', proxies=proxy)


def status_coderrr(r):
        print(" Print Status_Code ")
        print(r.status_code)


print(status_coderrr(r))
def head(r):
        print(" Print HEAD ")
        print(r.headers)

print(head(r))

def body(r):
        print(" Print BODY ")
        print(r.text)
print(body(r))

def session(r):
        s = requests.Session()
        s.proxies = proxy
        print(s.proxies)
        print(s)
print(session(r))
